var express = require('express');
var app = express();
var bodyParser = require('body-parser')
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;
var amqp = require('amqplib/callback_api');
var sha224 = require('js-sha256').sha224;
var uuid = require('uuid4');
var users = [];
var messages = [];

app.use(express.static('http'));
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

app.get('/', function(req, res){
  res.sendFile(__dirname + '/http/static/chat.html');
});

app.post('/login', function (req, res) {
  //save user login
  var userid = sha224(req.body.name);
  var guid = uuid();
  users.push([userid, guid]);
  //console.log(users);
});

io.on('connection', function(socket){
  //need jwt authentication
    socket.on('chat message', function(msg){
      messages.push(msg)
      sendToQueue(JSON.stringify(msg))
      //console.log(messages);
  });
});

function sendToQueue(msg) {
  amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {
      var q = 'raw';
      ch.assertQueue(q, {durable: false});
      ch.sendToQueue(q, new Buffer(msg));
    });
  });
}

function recvFromQueue() {
  amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {
      var q = 'clean';
      ch.assertQueue(q, {durable: false});
      ch.consume(q, function(msg) {
        io.emit('chat message', JSON.parse(msg.content));
      }, {noAck: true});
    });
  });
}


http.listen(port, function(){
  console.log('listening on *:' + port);
  recvFromQueue()
});
