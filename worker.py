#!/usr/bin/env python3

import json
import asyncio
import aioamqp


def filter_len(message):
    if len(message) >= 20:
        return False
    else:
        return True

def filter_bad(message):
    badwords = set(['cat', 'dog', 'cow'])
    for word in badwords:
        if word in message:
            return False
    return True


def cascade(message):
    if filter_len(message):
        if filter_bad(message):
            return True

@asyncio.coroutine
def send(message):
    transport, protocol = yield from aioamqp.connect('localhost')
    channel = yield from protocol.channel()
    yield from channel.queue_declare(queue_name='clean')
    yield from channel.basic_publish(
        payload=message,
        exchange_name='',
        routing_key='clean'
    )
    yield from protocol.close()
    transport.close()

@asyncio.coroutine
def callback(channel, body, envelope, properties):
    yield from do_work(body)

@asyncio.coroutine
def do_work(body):
    message = json.loads(body.decode('utf-8'))['msg']
    if cascade(message):
        yield from send(body)

@asyncio.coroutine
def receive():
    transport, protocol = yield from aioamqp.connect('localhost')
    channel = yield from protocol.channel()
    yield from channel.queue_declare(queue_name='raw')
    yield from channel.basic_consume(callback, queue_name='raw')


if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    event_loop.run_until_complete(asyncio.wait([receive()]))
    event_loop.run_forever()
